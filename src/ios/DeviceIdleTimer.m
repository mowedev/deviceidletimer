/********* DeviceIdleTimer.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "DeviceIdleTimerUIApplication.h"

@interface DeviceIdleTimer : CDVPlugin {
  // Member variables go here.
}

- (void)registerForIdleTimerNotification:(CDVInvokedUrlCommand*)command;
@end

@implementation DeviceIdleTimer

static NSString *notificationCallback = @"DeviceIdleTimer.onNotificationReceived";

- (void)registerForIdleTimerNotification:(CDVInvokedUrlCommand*)command
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationIdleTimerNotificationHandler:) name:kDeviceIdleTimerNotification object:nil];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)applicationIdleTimerNotificationHandler:(NSNotification *)notification
{
    NSString * notifyJS = [NSString stringWithFormat:@"%@(%@);", notificationCallback, @""];
    NSLog(@"stringByEvaluatingJavaScriptFromString %@", notifyJS);
    
    if ([self.webView respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)]) {
        [(UIWebView *)self.webView stringByEvaluatingJavaScriptFromString:notifyJS];
    } else {
        [self.webViewEngine evaluateJavaScript:notifyJS completionHandler:nil];
    }
}

@end
