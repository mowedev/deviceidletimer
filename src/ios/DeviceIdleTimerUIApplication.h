//
//  TIMERUIApplication.h
//  mylife@Magna Steyr
//
//  Created by Mina Haleem on 02/01/2017.
//
//

#import <Foundation/Foundation.h>

//the length of time before your application "times out". This number actually represents seconds, so we'll have to multiple it by 60 in the .m file
#define kApplicationTimeoutInMinutes 0.5

//the notification your AppDelegate needs to watch for in order to know that it has indeed "timed out"
#define kDeviceIdleTimerNotification @"DeviceIdleTimerNotification"

@interface DeviceIdleTimerUIApplication : UIApplication
{
    NSTimer     *myidleTimer;
}

@end
