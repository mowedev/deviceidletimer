# README #

### What is this repository for? ###

* Getting notification when the device is idel "Working for iOS only"
* This plugin require self modification to "main.m" file

### iOS Preparation ###
* Add DeviceIdleTimerUIApplication class as UIApplication
* Change main.m file to be like: 

```objc
#import <UIKit/UIKit.h>
#import "DeviceIdleTimerUIApplication.h"

int main(int argc, char* argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, NSStringFromClass([DeviceIdleTimerUIApplication class]), @"AppDelegate");
        return retVal;
    }
}
```

### Usage ###

```javascript
window.DeviceIdleTimer.registerForIdleTimerNotification(function(){
				console.log("application is idle");
});
```