var exec = require('cordova/exec');

function DeviceIdleTimer() {

}

DeviceIdleTimer.prototype.registerForIdleTimerNotification = function (callback, success, error) {
	console.log("DeviceIdleTimer Calling the function of registration");
    DeviceIdleTimer.prototype.onNotificationReceived = callback;
	exec(success, error, "DeviceIdleTimer", 'registerForIdleTimerNotification',[]);
}

// DEFAULT NOTIFICATION CALLBACK //
DeviceIdleTimer.prototype.onNotificationReceived = function(){
	console.log("Received idle timer notification");
}

var deviceIdleTimer = new DeviceIdleTimer();
module.exports = deviceIdleTimer;

